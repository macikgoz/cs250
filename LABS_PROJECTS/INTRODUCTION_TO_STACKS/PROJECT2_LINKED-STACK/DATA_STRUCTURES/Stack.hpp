#ifndef _STACK_HPP
#define _STACK_HPP

#include "Node.hpp"

template <typename T>
class LinkedStack
{
    public:
    LinkedStack()
    {
        m_ptrFirst = nullptr;
        m_ptrLast = nullptr;
        m_itemCount = 0;
    }

    void Push( const T& newData ) noexcept
    {
        // allocating memory
        Node<T>* newNode = new Node<T>;
        newNode->data = newData;

        // if the linkedstack is empty
        if (m_ptrLast == nullptr){
                m_ptrFirst = newNode;
                m_ptrLast  = newNode;
        }
        else{  // or it has at least one element
            newNode->ptrPrev = m_ptrLast;
            m_ptrLast->ptrNext = newNode;
            m_ptrLast = newNode;
        }
        // increment the number of elements in the LinkedStack in either way
        m_itemCount++;
    }

    T& Top()
    {
        if (Size() == 0)
            throw runtime_error("LinkedStack is empty!");
        else
            return m_ptrLast->data;
        //throw runtime_error( "Not yet implemented" );   // placeholder
    }

    void Pop()
    {
        // if it is empty
        if (m_itemCount == 0){
            return;
        }
        // if it has only one element
        else if(m_itemCount == 1) {
            delete m_ptrLast;
            m_ptrLast = nullptr;
            m_ptrFirst = nullptr;
        }
        // or if has more than one element
        else {
            Node<T>* ptrSecondtoLast = m_ptrLast->ptrPrev;

            delete m_ptrLast;

            m_ptrLast = ptrSecondtoLast;
            m_ptrLast->ptrNext = nullptr; 

        }
        m_itemCount --;
    }

    int Size()
    {
        return m_itemCount;    // placeholder
    }

    private:
    Node<T>* m_ptrFirst;
    Node<T>* m_ptrLast;
    int m_itemCount;
};

#endif
