#include "CourseCatalog.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stack>
using namespace std;

#include "UTILITIES/Menu.hpp"
#include "EXCEPTIONS/CourseNotFoundException.hpp"
#include "DATA_STRUCTURES/Stack.hpp"

CourseCatalog::CourseCatalog()
{
    LoadCourses();
}

void CourseCatalog::LoadCourses() // done
{
    Menu::Header( "LOADING COURSES" );

    ifstream input( "courses.txt" );

    if ( !input.is_open() )
    {
        cout << "Error opening input text file, courses.txt" << endl;
        return;
    }

    string label, courseCode, courseName, prerequisite;
    Course newCourse;

    while ( input >> label )
    {
        if ( label == "COURSE" )
        {
            if ( newCourse.name != "" )
            {
                m_courses.PushBack( newCourse );
                newCourse.Clear();
            }

            input >> newCourse.code >> newCourse.name;
        }
        else if ( label == "PREREQ" )
        {
            input >> newCourse.prereq;
        }
    }

    input.close();

    cout << " * " << m_courses.Size() << " courses loaded" << endl << endl;
}

void CourseCatalog::ViewCourses() noexcept
{
    Menu::Header( "VIEW COURSES" );
    cout <<  "#\t CODE\t\t TITLE" << endl;
    cout <<  " \t   PREREQS"  << endl;
    Menu::DrawHorizontalBar( 80 );

    for (int i= 0; i < m_courses.Size(); i++){
    //     cout << i << "\t" << m_courses.Get(i).code << "\t\t" << m_courses.Get(i).name<< endl;
         cout << i << "\t" << m_courses[i].code << "\t\t" << m_courses[i].name<< endl;
         if (m_courses.Get(i).prereq !="")
            cout << "\t  " << m_courses.Get(i).prereq << endl;
         cout << endl;
    }


}

Course CourseCatalog::FindCourse( const string& code )
{
    bool found = false;
    for ( int i = 0; i < m_courses.Size(); i++){
        if (m_courses[i].code == code){
                found = true;
                return m_courses.Get(i);
        }
    }

    if (!found)
        throw CourseNotFound( "Unable to find course " + code ) ;

}

void CourseCatalog::ViewPrereqs() noexcept
{
    Menu::Header( "GET PREREQS" );

    string courseCode;
    Course current;

    cout << endl <<"Enter class code " << endl << endl;
    cout << ">> ";
    cin >> courseCode;

    try {
        current = FindCourse(courseCode);
    }
    catch (runtime_error& ex){
        cout << endl << "Error! " << ex.what() <<"!" << endl;
        return ;

    }

        LinkedStack<Course> prereqs;
        prereqs.Push(current);

        while ( current.prereq != "" ) {
                try {
                    current = FindCourse(current.prereq);
                }
                catch(runtime_error){
                    break;
                }
                prereqs.Push(current);
        }

        cout << endl << "Classes to take: " << endl;

        int i = 0;
        while ( prereqs.Size() > 0 ) {
            i++;
            cout << i << ".  " << prereqs.Top().code << "   " <<prereqs.Top().name << endl;
            prereqs.Pop();
        }
}

void CourseCatalog::Run()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );

        int choice = Menu::ShowIntMenuWithPrompt( { "View all courses", "Get course prerequisites", "Exit" } );

        switch( choice )
        {
            case 1:
                ViewCourses();
            break;

            case 2:
                ViewPrereqs();
            break;

            case 3:
                done = true;
            break;
        }
    }
}
