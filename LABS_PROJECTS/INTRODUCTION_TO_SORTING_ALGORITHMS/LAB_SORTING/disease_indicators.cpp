#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <fstream>
#include <map>
using namespace std;

#include "Timer.hpp"
#include "Menu.hpp"

struct DataEntry
{
    map<string, string> fields;

    void Output( ofstream& output )
    {
        for ( map<string, string>::iterator it = fields.begin();
                it != fields.end(); it++ )
        {
            output << left << setw( 30 ) << it->second;
        }
        output << endl;
    }
};

void ReadData( vector<DataEntry>& data, const string& filename );

/* ********************************************* */
/* TODO: Add prototypes for sort functions here. */
/* ********************************************* */
void SelectionSort( vector<DataEntry>& data, const string& onKey );


void QuickSort_Recursive( vector<DataEntry>& data, int low, int high, int& iterations, const string& onKey );
int QuickSort( vector<DataEntry>& data, const string& onKey );
int Partition( vector<DataEntry>& data, int low, int high, int& iterations, const string& onKey );

void MergeSort_Recursive( vector<DataEntry>& data, int first, int last, int& iterations, const string& onKey );
void Merge( vector<DataEntry>& data, int first, int mid, int last, int& iterations, const string& onKey );
int MergeSort( vector<DataEntry>& data, const string& onKey );


int main()
{
    Menu::Header( "U.S. Chronic Disease Indicators" );

    vector<string> filenames = {
        "100_US_Chronic_Disease_Indicators.csv",
        "1000_US_Chronic_Disease_Indicators.csv",
        "10000_US_Chronic_Disease_Indicators.csv",
        "523487_US_Chronic_Disease_Indicators.csv"
    };

    /* ********************************************* */
    /* TODO: Update this menu with your sorting algs */
    /* ********************************************* */
    vector<string> sorts = {
        "Selection Sort",
        "Merge Sort",
        "Quick Sort"
    };

    vector<string> columns = {
        "YearStart", "YearEnd", "LocationAbbr", "LocationDesc", "Topic", "Question"
    };

    cout << "Which file do you want to load?" << endl;
    int fileChoice = Menu::ShowIntMenuWithPrompt( filenames );

    cout << "Which sort do you want to use?" << endl;
    int sortChoice = Menu::ShowIntMenuWithPrompt( sorts );

    cout << "Which column do you want to sort on?" << endl;
    int sortOnChoice = Menu::ShowIntMenuWithPrompt( columns );

    cout << fileChoice << ", " << sortChoice << ", " << sortOnChoice << endl;

    Timer timer;
    vector<DataEntry> data;

    string filename = filenames[ fileChoice - 1 ];


    // Read in the data from the file
    cout << left << setw( 15 ) << "BEGIN:" << "Loading data from file, \"" << filename << "\"..." << endl;
    timer.Start();
    ReadData( data, filename );
    cout << left << setw( 15 ) << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() << " milliseconds" << endl << endl;

    cout << data.size() << " items loaded" << endl;


    // Sort the data
    if ( sortChoice == 1 )
    {
        cout << left << setw( 15 ) << "BEGIN:" << "Sorting data with Selection Sort..." << endl;
        timer.Start();

        /* *************************************** */
        /* TODO: Call basic sorting algorithm here */
        /* *************************************** */
        // example:
        SelectionSort( data, columns[ sortOnChoice - 1 ] );
        cout << left << setw( 15 ) << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() << " milliseconds" << endl << endl;
    }
    else if ( sortChoice == 2 )
    {
        cout << left << setw( 15 ) << "BEGIN:" << "Sorting data with Merge Sort..." << endl;
        timer.Start();
        /* **************************************** */
        /* TODO: Call faster sorting algorithm here */
        /* **************************************** */
        // example:
        MergeSort( data, columns[ sortOnChoice - 1 ] );
        cout << left << setw( 15 ) << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() << " milliseconds" << endl << endl;
    }
    else if (sortChoice ==3)
    {
        cout << left << setw( 15 ) << "BEGIN:" << "Sorting data with Quick Sort..." << endl;
        timer.Start();
        /* **************************************** */
        /* TODO: Call faster sorting algorithm here */
        /* **************************************** */
        // example:
        QuickSort(data, columns[ sortOnChoice - 1 ]);
        cout << left << setw( 15 ) << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() << " milliseconds" << endl << endl;
    }

    cout << endl << "Writing list out to \"output.txt\"..." << endl;


    // Output the sorted data
    ofstream output( "output.txt" );
    for ( map<string, string>::iterator it = data[0].fields.begin();
            it != data[0].fields.end(); it++ )
    {
        output << left << setw( 30 ) << it->first;
    }

    output << endl;

    for ( unsigned int i = 0; i < data.size(); i++ )
    {
        data[i].Output( output );
    }
    output.close();

    cout << endl;

    Menu::Header( "Hit ENTER to quit." );

    cin.ignore();
    cin.get();

    return 0;
}

void ReadData( vector<DataEntry>& data, const string& filename )
{
    ifstream input( filename );

    vector<string> headerItems;

    unsigned int field = 0;

    string line;
    bool header = true;
    bool skippedGeoComma = false;
    while ( getline( input, line ) )
    {
        DataEntry entry;

        int columnBegin = 0;
        field = 0;
        skippedGeoComma = false;

        for ( unsigned int i = 0; i < line.size(); i++ )
        {
            //cout << line[i];
            if ( line[i] == ',' )
            {
                int length = i - columnBegin;
                string substring = line.substr( columnBegin, length );

                if ( header )
                {
                    headerItems.push_back( substring );
                }
                else
                {
                    string fieldKey = "";
                    if (field >= headerItems.size())
                    {
                        fieldKey = "Unknown";
                    }
                    else
                    {
                        fieldKey = headerItems[field];
                    }
                    entry.fields[ fieldKey ] = substring;
                }

                columnBegin = i+1;

                if ( header == false && skippedGeoComma == false && headerItems[ field ] == "GeoLocation" )
                {
                    skippedGeoComma = true;
                    // Ignore this comma.
                    continue;
                }
                else
                {
                    field++;
                }
            }
        }

        if ( header )
        {
            header = false;
        }
        else
        {
            data.push_back( entry );
        }
    }

    input.close();
}

/* ******************************************** */
/* TODO: Implement sorting algorithsm down here */
/* ******************************************** */

void SelectionSort( vector<DataEntry>& data, const string& onKey )
{
    int n = data.size();

    for ( int j = 0; j < n-1; j++ )
    {
        int iMin = j;
        for ( int i = j+1; i < n; i++ )
        {
            if ( data[i].fields[ onKey ] < data[iMin].fields[ onKey ] )
            {
                iMin = i;
            }

            if ( iMin != j )
            {
                DataEntry temp = data[j];
                data[j] = data[iMin];
                data[iMin] = temp;
            }
        }
    }
}


///// Quick sort
// Implementation from GeeksForGeeks
// https://www.geeksforgeeks.org/quick-sort/
void QuickSort_Recursive( vector<DataEntry>& data, int low, int high, int& iterations, const string& onKey )
{
    iterations++;

    if ( low < high )
    {
        int partitionIndex = Partition( data, low, high, iterations, onKey );

        QuickSort_Recursive( data, low, partitionIndex - 1, iterations, onKey );
        QuickSort_Recursive( data, partitionIndex + 1, high, iterations, onKey  );
    }
}

int QuickSort( vector<DataEntry>& data,const string& onKey )
{
    int iterations = 0;

    QuickSort_Recursive( data, 0, data.size()-1, iterations, onKey );

    return iterations;
}

int Partition( vector<DataEntry>& data, int low, int high, int& iterations, const string& onKey )
{
    DataEntry pivot = data[high];

    int i = (low - 1);

    for ( int j = low; j <= high - 1; j++ )
    {
        if ( data[j].fields[ onKey ] <= pivot.fields[ onKey ] )
        {

            i++;

            DataEntry temp = data[i];
            data[i] = data[j];
            data[j] = temp;
        }
        iterations++;
    }

    DataEntry temp = data[i + 1];
    data[i + 1] = data[high];
    data[high] = temp;

    return ( i + 1 );
}


///  Merge Sort

// Implementation from Data Abstraction & Problem Solving with Walls and Mirrors, 7th ed
// Frank Carrano and Timothy Henry
void MergeSort_Recursive( vector<DataEntry>& data, int first, int last, int& iterations, const string& onKey )
{
    iterations++;

    if ( first < last )
    {
        int mid = first + (last - first)/2;

        MergeSort_Recursive( data, first, mid, iterations, onKey );
        MergeSort_Recursive( data, mid+1, last, iterations, onKey );
        Merge( data, first, mid, last, iterations, onKey );
    }
}

void Merge( vector<DataEntry>& data, int first, int mid, int last, int& iterations, const string& onKey )
{
    vector<DataEntry> temp = data;

    int first1 = first;
    int last1 = mid;
    int first2 = mid+1;
    int last2 = last;

    int index = first;
    while ( ( first1 <= last1 ) && ( first2 <= last2 ) )
    {
        if ( data[first1].fields[ onKey ] <= data[first2].fields[ onKey ] )
        {
            temp[index] = data[first1];
            first1++;
        }
        else
        {
            temp[index] = data[first2];
            first2++;
        }
        index++;
        iterations++;
    }

    while ( first1 <= last1 )
    {
        temp[index] = data[first1];
        first1++;
        index++;
        iterations++;
    }

    while ( first2 <= last2 )
    {
        temp[index] = data[first2];
        first2++;
        index++;
        iterations++;
    }

    data = temp;
}

int MergeSort( vector<DataEntry>& data, const string& onKey )
{
    int iterations = 0;

    MergeSort_Recursive( data, 0, data.size()-1, iterations, onKey );

    return iterations;
}




