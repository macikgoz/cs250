#include "Tester.hpp"

#define ARRAY_NAME items

template <typename T>
void Tester::Debug_ListItems( List<T> item )
{
    cout << endl << "CONTENTS: " << endl;
    for ( int i = 0; i < item.Size(); i++ )
    {
        cout << i << "\t" << item.Get( i ) << endl;
    }
}

void Tester::RunTests()
{
    // Don't modify this function

    // STUDENT TESTS
    cout << endl << "STUDENT TESTS" << endl;
    Test_IsEmpty();
    Test_IsFull();
    Test_Size();
    Test_GetCountOf();
    Test_Contains();
    Test_PushFront();
    Test_PushBack();
    Test_Get();
    Test_GetFront();
    Test_GetBack();
    Test_PopFront();
    Test_PopBack();
    Test_Clear();
    Test_ShiftRight();
    Test_ShiftLeft();
//    Test_RemoveItem();
//    Test_RemoveIndex();
Test_RemoveWithValue();
Test_RemoveAtIndex();


    // INSTRUCTOR TESTS
    cout << endl << "INSTRUCTOR TESTS" << endl;

    DrawLine(); cout << "TEST: IsEmpty" << endl;

    Test_IsEmpty_Rachel();

    DrawLine(); cout << "TEST: IsFull" << endl;

    Test_IsFull_Rachel();

    DrawLine(); cout << "TEST: Size" << endl;

    Test_Size_Rachel();

    DrawLine(); cout << "TEST: GetCountOf" << endl;

    Test_GetCountOf_Rachel();

    DrawLine(); cout << "TEST: Contains" << endl;

    Test_Contains_Rachel();

    DrawLine(); cout << "TEST: PushFront" << endl;

    Test_PushFront_Rachel();

    DrawLine(); cout << "TEST: PushBack" << endl;

    Test_PushBack_Rachel();

    DrawLine(); cout << "TEST: Get" << endl;

    Test_Get_Rachel();

    DrawLine(); cout << "TEST: GetFront" << endl;

    Test_GetFront_Rachel();

    DrawLine(); cout << "TEST: GetBack" << endl;

    Test_GetBack_Rachel();

    DrawLine(); cout << "TEST: PopFront" << endl;

    Test_PopFront_Rachel();

    DrawLine(); cout << "TEST: PopBack" << endl;

    Test_PopBack_Rachel();

    DrawLine(); cout << "TEST: Clear" << endl;

    Test_Clear_Rachel();

    DrawLine(); cout << "TEST: ShiftRight" << endl;

    Test_ShiftRight_Rachel();

    DrawLine(); cout << "TEST: ShiftLeft" << endl;

    Test_ShiftLeft_Rachel();

    DrawLine(); cout << "TEST: RemoveItem" << endl;

    Test_RemoveItem_Rachel();

    DrawLine(); cout << "TEST: RemoveIndex" << endl;

    Test_RemoveIndex_Rachel();

    DrawLine(); cout << "TEST: Insert" << endl;

    Test_Insert_Rachel();
}

void Tester::DrawLine()
{
    cout << endl;
    for ( int i = 0; i < 80; i++ )
    {
        cout << "-";
    }
    cout << endl;
}

void Tester::Test_Init_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - Create empty list, check Size()" << endl;
        cout << "\t Prereqs: PushBack(), Size()" << endl;
        List<int> testList;
        int expectedSize = 0;
        int actualSize = testList.Size();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << "\t";

        if ( actualSize == expectedSize )   { cout << "\n\t PASS" << endl; }
        else                                { cout << "\n\t FAIL" << endl; }
    }   // Test end
}

void Tester::Test_ShiftRight_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - PushBack abc, ShiftRight at 0, check values" << endl;
        cout << "\t Prereqs: PushBack(), ShiftRight(), Size()" << endl;

        List<string> testList;
        testList.PushBack( "a" );
        testList.PushBack( "b" );
        testList.PushBack( "c" );

        testList.ShiftRight( 0 );

        // We don"t care what's at position 0
        string expectedValues[4] = { " ", "a", "b", "c" };
        string actualValues[4];

        actualValues[1] = testList.ARRAY_NAME[1];
        actualValues[2] = testList.ARRAY_NAME[2];
        actualValues[3] = testList.ARRAY_NAME[3];

        // Size should remain the same
        int expectedSize = 3;
        int actualSize = testList.Size();
        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << endl;

        bool allMatch = true;
        for ( int i = 0; i < 4; i++ )
        {
            if ( i == 0 ) {
                cout << "\t Value at " << i << " doesn't matter." << endl;
                continue;
            } // don't care

            if ( actualValues[i] != expectedValues[i] )
            {
                allMatch = false;
            }
            cout << "\t Expected value at " << i << ": " << expectedValues[i] << "\t Actual value: " << actualValues[i] << endl;
        }

        if ( actualSize != expectedSize )   { cout << "\n\t FAIL - Size is wrong; shouldn't change!" << endl; }
        else if ( !allMatch )               { cout << "\n\t FAIL - Values didn't match!" << endl; }
        else                                { cout << "\n\t PASS" << endl; }
    }   // Test end

    {   // Test begin
        cout << endl << "Rachel Test 2 - PushBack abc, ShiftRight at 1, check values" << endl;
        cout << "\t Prereqs: PushBack(), ShiftRight(), Size()" << endl;

        List<string> testList;
        testList.PushBack( "a" );
        testList.PushBack( "b" );
        testList.PushBack( "c" );

        testList.ShiftRight( 1 );

        // We don't care what's at the shifted position
        string expectedValues[4] = { "a", " ", "b", "c" };
        string actualValues[4];

        actualValues[0] = testList.ARRAY_NAME[0];
        actualValues[2] = testList.ARRAY_NAME[2];
        actualValues[3] = testList.ARRAY_NAME[3];

        // Size should remain the same
        int expectedSize = 3;
        int actualSize = testList.Size();
        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << endl;

        bool allMatch = true;
        for ( int i = 0; i < 4; i++ )
        {
            if ( i == 1 ) {
                cout << "\t Value at " << i << " doesn't matter." << endl;
                continue;
            } // don't care

            if ( actualValues[i] != expectedValues[i] )
            {
                allMatch = false;
            }
            cout << "\t Expected value at " << i << ": " << expectedValues[i] << "\t Actual value: " << actualValues[i] << endl;
        }


        if ( actualSize != expectedSize )   { cout << "\n\t FAIL - Size is wrong; shouldn't change!" << endl; }
        else if ( !allMatch )               { cout << "\n\t FAIL - Values didn't match!" << endl; }
        else                                { cout << "\n\t PASS" << endl; }
    }   // Test end
}

void Tester::Test_ShiftLeft_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - PushBack abc, ShiftLeft at 0, check values" << endl;
        cout << "\t Prereqs: PushBack(), ShiftLeft(), Size()" << endl;

        List<string> testList;
        testList.PushBack( "a" );
        testList.PushBack( "b" );
        testList.PushBack( "c" );

        testList.ShiftLeft( 0 );

        string expectedValues[4] = { "b", "c", " ", " " };
        string actualValues[4];

        actualValues[0] = testList.ARRAY_NAME[0];
        actualValues[1] = testList.ARRAY_NAME[1];

        // Size should remain the same
        int expectedSize = 3;
        int actualSize = testList.Size();
        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << endl;

        bool allMatch = true;
        for ( int i = 0; i < 2; i++ )
        {
            if ( actualValues[i] != expectedValues[i] )
            {
                allMatch = false;
            }
            cout << "\t Expected value at " << i << ": " << expectedValues[i] << "\t Actual value: " << actualValues[i] << endl;
        }


        if ( actualSize != expectedSize )   { cout << "\n\t FAIL - Size is wrong; shouldn't change!" << endl; }
        else if ( !allMatch )               { cout << "\n\t FAIL - Values didn't match!" << endl; }
        else                                { cout << "\n\t PASS" << endl; }
    }   // Test end

    {   // Test begin
        cout << endl << "Rachel Test 2 - PushBack abc, ShiftLeft at 1, check values" << endl;
        cout << "\t Prereqs: PushBack(), ShiftLeft(), Size()" << endl;

        List<string> testList;
        testList.PushBack( "a" );
        testList.PushBack( "b" );
        testList.PushBack( "c" );

        testList.ShiftLeft( 1 );

        string expectedValues[4] = { "a", "c", " ", " " };
        string actualValues[4];

        actualValues[0] = testList.ARRAY_NAME[0];
        actualValues[1] = testList.ARRAY_NAME[1];

        // Size should remain the same
        int expectedSize = 3;
        int actualSize = testList.Size();
        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << endl;

        bool allMatch = true;
        for ( int i = 0; i < 2; i++ )
        {
            if ( actualValues[i] != expectedValues[i] )
            {
                allMatch = false;
            }
            cout << "\t Expected value at " << i << ": " << expectedValues[i] << "\t Actual value: " << actualValues[i] << endl;
        }


        if ( actualSize != expectedSize )   { cout << "\n\t FAIL - Size is wrong; shouldn't change!" << endl; }
        else if ( !allMatch )               { cout << "\n\t FAIL - Values didn't match!" << endl; }
        else                                { cout << "\n\t PASS" << endl; }
    }   // Test end
}

void Tester::Test_Size_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - Create empty list, check Size()" << endl;
        cout << "\t Prereqs: PushBack(), Size()" << endl;
        List<int> testList;
        int expectedSize = 0;
        int actualSize = testList.Size();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << "\t";

        if ( actualSize == expectedSize )   { cout << "\n\t PASS" << endl; }
        else                                { cout << "\n\t FAIL" << endl; }
    }   // Test end

    {   // Test begin
        cout << endl << "Rachel Test 2 - Create list, add 2 items, check Size()" << endl;
        cout << "\t Prereqs: PushBack(), Size()" << endl;
        List<int> testList;

        testList.PushBack( 1 );
        testList.PushBack( 3 );

        int expectedSize = 2;
        int actualSize = testList.Size();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << "\t";

        if ( actualSize == expectedSize )   { cout << "\n\t PASS" << endl; }
        else                                { cout << "\n\t FAIL" << endl; }
    }   // Test end

    {   // Test begin
        cout << endl << "Rachel Test 3 - Create list, add 105 items, check Size()" << endl;
        cout << "\t Prereqs: PushBack(), Size()" << endl;
        List<int> testList;

        for ( int i = 0; i < 105; i++ ) { testList.PushBack( i ); }

        int expectedSize = 100;
        int actualSize = testList.Size();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << "\t";

        if ( actualSize == expectedSize )   { cout << "\n\t PASS" << endl; }
        else                                { cout << "\n\t FAIL" << endl; }
    }   // Test end
}

void Tester::Test_IsEmpty_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - Create empty list, check IsEmpty()" << endl;
        cout << "\t Prereqs: PushBack(), IsEmpty()" << endl;

        List<int> testList;
        bool expectedIsEmpty = true;
        bool actualIsEmpty = testList.IsEmpty();

        cout << "\t Expected IsEmpty(): " << expectedIsEmpty << "\t Actual IsEmpty():   " << actualIsEmpty << "\t";

        if ( actualIsEmpty == expectedIsEmpty )     { cout << "\n\t PASS" << endl; }
        else                                        { cout << "\n\t FAIL" << endl; }
    }   // Test end

    {   // Test begin
        cout << endl << "Rachel Test 2 - Create list, add items, check IsEmpty()" << endl;
        cout << "\t Prereqs: PushBack(), IsEmpty()" << endl;

        List<int> testList;
        testList.PushBack( 1 );
        testList.PushBack( 2 );
        testList.PushBack( 3 );

        bool expectedIsEmpty = false;
        bool actualIsEmpty = testList.IsEmpty();

        cout << "\t Expected IsEmpty(): " << expectedIsEmpty << "\t Actual IsEmpty():   " << actualIsEmpty << "\t";

        if ( actualIsEmpty == expectedIsEmpty )     { cout << "\n\t PASS" << endl; }
        else                                        { cout << "\n\t FAIL" << endl; }
    }   // Test end

    {   // Test begin
        cout << endl << "Rachel Test 2 - Create list, add 105 items, check IsEmpty()" << endl;
        cout << "\t Prereqs: PushBack(), IsEmpty()" << endl;

        List<int> testList;
        for ( int i = 0; i < 105; i++ ) { testList.PushBack( i ); }

        bool expectedIsEmpty = false;
        bool actualIsEmpty = testList.IsEmpty();

        cout << "\t Expected IsEmpty(): " << expectedIsEmpty << "\t Actual IsEmpty():   " << actualIsEmpty << "\t";

        if ( actualIsEmpty == expectedIsEmpty )     { cout << "\n\t PASS" << endl; }
        else                                        { cout << "\n\t FAIL" << endl; }
    }   // Test end
}

void Tester::Test_IsFull_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - Create empty list, check IsFull()" << endl;
        cout << "\t Prereqs: PushBack(), IsFull()" << endl;

        List<int> testList;
        bool expectedIsFull = false;
        bool actualIsFull = testList.IsFull();

        cout << "\t Expected IsFull(): " << expectedIsFull << "\t Actual IsFull():   " << actualIsFull << "\t";

        if ( actualIsFull == expectedIsFull )     { cout << "\n\t PASS" << endl; }
        else                                      { cout << "\n\t FAIL" << endl; }
    }   // Test end

    {   // Test begin
        cout << endl << "Rachel Test 2 - Create list, add items, check IsFull()" << endl;
        cout << "\t Prereqs: PushBack(), IsFull()" << endl;

        List<int> testList;
        testList.PushBack( 1 );
        testList.PushBack( 2 );
        testList.PushBack( 3 );

        bool expectedIsFull = false;
        bool actualIsFull = testList.IsFull();

        cout << "\t Expected IsFull(): " << expectedIsFull << "\t Actual IsFull():   " << actualIsFull << "\t";

        if ( actualIsFull == expectedIsFull )     { cout << "\n\t PASS" << endl; }
        else                                      { cout << "\n\t FAIL" << endl; }
    }   // Test end

    {   // Test begin
        cout << endl << "Rachel Test 2 - Create list, add 100 items, check IsFull()" << endl;
        cout << "\t Prereqs: PushBack(), IsFull()" << endl;

        List<int> testList;
        for ( int i = 0; i < 100; i++ ) { testList.PushBack( i ); }

        bool expectedIsFull = true;
        bool actualIsFull = testList.IsFull();

        cout << "\t Expected IsFull(): " << expectedIsFull << "\t Actual IsFull():   " << actualIsFull << "\t";

        if ( actualIsFull == expectedIsFull )     { cout << "\n\t PASS" << endl; }
        else                                      { cout << "\n\t FAIL" << endl; }
    }   // Test end

    {   // Test begin
        cout << endl << "Rachel Test 2 - Create list, add 105 items, check IsFull()" << endl;
        cout << "\t Testing this because we want to make sure something \n\t doesn't get changed mistakenly on a failed Push" << endl;
        cout << "\t Prereqs: PushBack(), IsFull()" << endl;

        List<int> testList;
        for ( int i = 0; i < 105; i++ ) { testList.PushBack( i ); }

        bool expectedIsFull = true;
        bool actualIsFull = testList.IsFull();

        cout << "\t Expected IsFull(): " << expectedIsFull << "\t Actual IsFull():   " << actualIsFull << "\t";

        if ( actualIsFull == expectedIsFull )     { cout << "\n\t PASS" << endl; }
        else                                      { cout << "\n\t FAIL" << endl; }
    }   // Test end
}

void Tester::Test_PushFront_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - PushFront 1 item, check Size(), check GetFront()" << endl;
        cout << "\t Prereqs: PushFront(), Size(), GetFront()" << endl;

        List<string> testList;
        testList.PushFront( "a" );

        int expectedSize = 1;
        int actualSize = testList.Size();

        string expectedFront = "a";
        if ( testList.GetFront() == nullptr ) { throw runtime_error( "nullptr encountered!" ); }
        string actualFront = *testList.GetFront();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << "\n";
        cout << "\t Expected GetFront(): " << expectedFront << "\t Actual GetFront():   " << actualFront << "\n";

        if      ( actualSize != expectedSize )      { cout << "\n\t FAIL - Size was wrong" << endl; }
        else if ( actualFront != expectedFront )    { cout << "\n\t FAIL - Front value was wrong" << endl; }
        else                                        { cout << "\n\t PASS" << endl; }
    }   // Test end


    {   // Test begin
        cout << endl << "Rachel Test 2 - PushFront 3 items, check Size(), check GetFront()" << endl;
        cout << "\t Prereqs: PushFront(), Size(), GetFront()" << endl;

        List<string> testList;

        string expectedFronts[3] = { "a", "b", "c" };
        string actualFronts[3];

        testList.PushFront( "a" );
        if ( testList.GetFront() == nullptr ) { throw runtime_error( "nullptr encountered!" ); }
        actualFronts[0] = *testList.GetFront();

        testList.PushFront( "b" );
        if ( testList.GetFront() == nullptr ) { throw runtime_error( "nullptr encountered!" ); }
        actualFronts[1] = *testList.GetFront();

        testList.PushFront( "c" );
        if ( testList.GetFront() == nullptr ) { throw runtime_error( "nullptr encountered!" ); }
        actualFronts[2] = *testList.GetFront();

        int expectedSize = 3;
        int actualSize = testList.Size();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << "\n";

        bool allMatch = true;
        for ( int i = 0; i < 3; i++ )
        {
            cout << "\t Expected GetFront(): " << expectedFronts[i] << "\t Actual GetFront():   " << actualFronts[i] << "\n";
            if ( actualFronts[i] != expectedFronts[i] ) { allMatch = false; }
        }

        if      ( actualSize != expectedSize )      { cout << "\n\t FAIL - Wrong size" << endl; }
        else if ( !allMatch )                       { cout << "\n\t FAIL - Values don't match" << endl; }
        else                                        { cout << "\n\t PASS" << endl; }

        cout << endl << "Value check:" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << i << "\t" << *testList.Get(i) << endl;
        }

    }   // Test end
}

void Tester::Test_PushBack_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - PushBack 1 item, check Size(), check GetBack()" << endl;
        cout << "\t Prereqs: PushBack(), Size(), GetBack()" << endl;

        List<string> testList;
        testList.PushBack( "a" );

        int expectedSize = 1;
        int actualSize = testList.Size();

        string expectedBack = "a";
        if ( testList.GetBack() == nullptr ) { throw runtime_error( "nullptr encountered!" ); }
        string actualBack = *testList.GetBack();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << "\n";
        cout << "\t Expected GetBack(): " << expectedBack << "\t Actual GetBack():   " << actualBack << "\n";

        if      ( actualSize != expectedSize )      { cout << "\n\t FAIL - Bad size" << endl; }
        else if ( actualBack != expectedBack )      { cout << "\n\t FAIL - Wrong back value" << endl; }
        else                                        { cout << "\n\t PASS" << endl; }
    }   // Test end


    {   // Test begin
        cout << endl << "Rachel Test 2 - PushBack 3 items, check Size(), check GetBack()" << endl;
        cout << "\t Prereqs: PushBack(), Size(), GetBack()" << endl;

        List<string> testList;

        string expectedBacks[3] = { "a", "b", "c" };
        string actualBacks[3];

        testList.PushBack( "a" );
        if ( testList.GetBack() == nullptr ) { throw runtime_error( "nullptr encountered!" ); }
        actualBacks[0] = *testList.GetBack();

        testList.PushBack( "b" );
        if ( testList.GetBack() == nullptr ) { throw runtime_error( "nullptr encountered!" ); }
        actualBacks[1] = *testList.GetBack();

        testList.PushBack( "c" );
        if ( testList.GetBack() == nullptr ) { throw runtime_error( "nullptr encountered!" ); }
        actualBacks[2] = *testList.GetBack();

        int expectedSize = 3;
        int actualSize = testList.Size();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << "\n";

        bool allMatch = true;
        for ( int i = 0; i < 3; i++ )
        {
            cout << "\t Expected GetBack(): " << expectedBacks[i] << "\t Actual GetBack():   " << actualBacks[i] << "\n";
            if ( actualBacks[i] != expectedBacks[i] ) { allMatch = false; }
        }

        if      ( actualSize != expectedSize )      { cout << "\n\t FAIL - Wrong size" << endl; }
        else if ( !allMatch )                       { cout << "\n\t FAIL - Values don't match" << endl; }
        else                                        { cout << "\n\t PASS" << endl; }
    }   // Test end
}

void Tester::Test_PopFront_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - PushBack 3 items, make sure correct item removed and Size() updated" << endl;
        cout << "\t Prereqs: PushBack(), Size(), PopFront(), GetFront()" << endl;

        List<string> testList;
        testList.PushBack( "a" );
        testList.PushBack( "b" );
        testList.PushBack( "c" );

        testList.PopFront();

        int expectedSize = 2;
        int actualSize = testList.Size();

        string expectedFront = "b";
        if ( testList.GetFront() == nullptr ) { throw runtime_error( "nullptr encountered!" ); }
        string actualFront = *testList.GetFront();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << "\n";
        cout << "\t Expected GetFront(): " << expectedFront << "\t Actual GetFront():   " << actualFront << "\n";

        if      ( actualSize != expectedSize )      { cout << "\n\t FAIL - Size was wrong" << endl; }
        else if ( actualFront != expectedFront )    { cout << "\n\t FAIL - Front value is wrong" << endl; }
        else                                        { cout << "\n\t PASS" << endl; }
    }   // Test end
}

void Tester::Test_PopBack_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - PushBack 3 items, make sure correct item removed and Size() updated" << endl;
        cout << "\t Prereqs: PushBack(), Size(), PopBack(), GetBack()" << endl;

        List<string> testList;
        testList.PushBack( "a" );
        testList.PushBack( "b" );
        testList.PushBack( "c" );

        testList.PopBack();

        int expectedSize = 2;
        int actualSize = testList.Size();

        string expectedBack = "b";
        if ( testList.GetBack() == nullptr ) { throw runtime_error( "nullptr encountered!" ); }
        string actualBack = *testList.GetBack();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << "\n";
        cout << "\t Expected GetBack(): " << expectedBack << "\t Actual GetBack():   " << actualBack << "\n";

        if      ( actualSize != expectedSize )      { cout << "\n\t FAIL - Size was wrong" << endl; }
        else if ( actualBack != expectedBack )      { cout << "\n\t FAIL - Back value is wrong" << endl; }
        else                                        { cout << "\n\t PASS" << endl; }
    }   // Test end
}

void Tester::Test_Clear_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - PushBack 3 items, make sure Size() is 0 after Clear()" << endl;
        cout << "\t Prereqs: PushBack(), Size(), Clear()" << endl;

        List<string> testList;
        testList.PushBack( "a" );
        testList.PushBack( "b" );
        testList.PushBack( "c" );

        testList.Clear();

        int expectedSize = 0;
        int actualSize = testList.Size();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << "\n";

        if      ( actualSize != expectedSize )      { cout << "\n\t FAIL - Size was wrong" << endl; }
        else                                        { cout << "\n\t PASS" << endl; }
    }   // Test end
}

void Tester::Test_Get_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - PushBack 3 items, check position on all" << endl;
        cout << "\t Prereqs: PushBack(), Get()" << endl;

        List<string> testList;
        testList.PushBack( "a" );
        testList.PushBack( "b" );
        testList.PushBack( "c" );

        string expectedValues[3] = { "a", "b", "c" };
        string actualValues[3];

        bool allMatch = true;
        for ( int i = 0; i < 3; i++ )
        {
            actualValues[i] = testList.ARRAY_NAME[i];

            cout << "\t Expected value at " << i << ": " << expectedValues[i] << "\t Actual value:   " << actualValues[i] << "\n";
            if ( actualValues[i] != expectedValues[i] )
            {
                allMatch = false;
            }
        }

        if      ( !allMatch )      { cout << "\n\t FAIL - Values don't match" << endl; }
        else                       { cout << "\n\t PASS" << endl; }
    }   // Test end

    {   // Test begin
        cout << endl << "Rachel Test 2 - PushFront 3 items, check position on all" << endl;
        cout << "\t Prereqs: PushFront(), Get()" << endl;

        List<string> testList;
        testList.PushFront( "a" );
        testList.PushFront( "b" );
        testList.PushFront( "c" );

        string expectedValues[3] = { "c", "b", "a" };
        string actualValues[3];

        bool allMatch = true;
        for ( int i = 0; i < 3; i++ )
        {
            actualValues[i] = testList.ARRAY_NAME[i];

            cout << "\t Expected value at " << i << ": " << expectedValues[i] << "\t Actual value:   " << actualValues[i] << "\n";
            if ( actualValues[i] != expectedValues[i] )
            {
                allMatch = false;
                break;
            }
        }

        if      ( !allMatch )      { cout << "\n\t FAIL - Values don't match" << endl; }
        else                       { cout << "\n\t PASS" << endl; }
    }   // Test end
}

void Tester::Test_GetFront_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - PushFront 3 items and check GetFront() each time." << endl;
        cout << "\t Prereqs: PushFront(), GetFront()" << endl;

        List<string> testList;

        string expectedFronts[3];
        string actualFronts[3];
        string* buffer;

        testList.PushFront( "a" );
        expectedFronts[0] = "a";
        if ( testList.GetFront() == nullptr ) { throw runtime_error( "nullptr encountered!" ); }
        actualFronts[0] = *testList.GetFront();

        testList.PushFront( "b" );
        expectedFronts[1] = "b";
        if ( testList.GetFront() == nullptr ) { throw runtime_error( "nullptr encountered!" ); }
        actualFronts[1] = *testList.GetFront();

        testList.PushFront( "c" );
        expectedFronts[2] = "c";
        if ( testList.GetFront() == nullptr ) { throw runtime_error( "nullptr encountered!" ); }
        actualFronts[2] = *testList.GetFront();


        bool allMatch = true;
        for ( int i = 0; i < 3; i++ )
        {
            cout << "\t Expected value at " << i << ": " << expectedFronts[i] << "\t Actual value:   " << actualFronts[i] << "\n";

            if ( actualFronts[i] != expectedFronts[i] )
            {
                allMatch = false;
            }
        }

        if      ( !allMatch )      { cout << "\n\t FAIL - Values don't match" << endl; }
        else                       { cout << "\n\t PASS" << endl; }
    }   // Test end
}

void Tester::Test_GetBack_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - PushBack 3 items and check GetBack() each time." << endl;
        cout << "\t Prereqs: PushBack(), GetBack()" << endl;

        List<string> testList;

        string expectedBacks[3];
        string actualBacks[3];
        string* buffer;

        testList.PushBack( "a" );
        expectedBacks[0] = "a";
        if ( testList.GetBack() == nullptr ) { throw runtime_error( "nullptr encountered!" ); }
        actualBacks[0] = *testList.GetBack();

        testList.PushBack( "b" );
        expectedBacks[1] = "b";
        if ( testList.GetBack() == nullptr ) { throw runtime_error( "nullptr encountered!" ); }
        actualBacks[1] = *testList.GetBack();

        testList.PushBack( "c" );
        expectedBacks[2] = "c";
        if ( testList.GetBack() == nullptr ) { throw runtime_error( "nullptr encountered!" ); }
        actualBacks[2] = *testList.GetBack();


        bool allMatch = true;
        for ( int i = 0; i < 3; i++ )
        {
            cout << "\t Expected value at " << i << ": " << expectedBacks[i] << "\t Actual value:   " << actualBacks[i] << "\n";

            if ( actualBacks[i] != expectedBacks[i] )
            {
                allMatch = false;
            }
        }

        if      ( !allMatch )      { cout << "\n\t FAIL - Values don't match" << endl; }
        else                       { cout << "\n\t PASS" << endl; }
    }   // Test end
}

void Tester::Test_GetCountOf_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - PushBack mississippi, check count of i." << endl;
        cout << "\t Prereqs: PushBack(), GetCountOf()" << endl;

        List<string> testList;
        testList.PushBack( "m" );
        testList.PushBack( "i" );
        testList.PushBack( "s" );
        testList.PushBack( "s" );
        testList.PushBack( "i" );
        testList.PushBack( "p" );
        testList.PushBack( "p" );
        testList.PushBack( "i" );

        int expectedCount = 3;
        int actualCount = testList.GetCountOf( "i" );

        cout << "\t Expected GetCountOf(): " << expectedCount << "\t Actual GetCountOf():   " << actualCount << "\n";

        if      ( actualCount != expectedCount )    { cout << "\n\t FAIL - Count was wrong" << endl; }
        else                                        { cout << "\n\t PASS" << endl; }
    }   // Test end

    {   // Test begin
        cout << endl << "Rachel Test 2 - PushBack mississippi, check count of z." << endl;
        cout << "\t Prereqs: PushBack(), GetCountOf()" << endl;

        List<string> testList;
        testList.PushBack( "m" );
        testList.PushBack( "i" );
        testList.PushBack( "s" );
        testList.PushBack( "s" );
        testList.PushBack( "i" );
        testList.PushBack( "p" );
        testList.PushBack( "p" );
        testList.PushBack( "i" );

        int expectedCount = 0;
        int actualCount = testList.GetCountOf( "z" );

        cout << "\t Expected GetCountOf(): " << expectedCount << "\t Actual GetCountOf():   " << actualCount << "\n";

        if      ( actualCount != expectedCount )    { cout << "\n\t FAIL - Count was wrong" << endl; }
        else                                        { cout << "\n\t PASS" << endl; }
    }   // Test end
}

void Tester::Test_Contains_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - PushBack mississippi, check Contains() of i." << endl;
        cout << "\t Prereqs: PushBack(), GetCountOf()" << endl;

        List<string> testList;
        testList.PushBack( "m" );
        testList.PushBack( "i" );
        testList.PushBack( "s" );
        testList.PushBack( "s" );
        testList.PushBack( "i" );
        testList.PushBack( "p" );
        testList.PushBack( "p" );
        testList.PushBack( "i" );

        bool expectedContains = true;
        bool actualContains = testList.Contains( "i" );

        cout << "\t Expected Contains(): " << expectedContains << "\t Actual Contains():   " << actualContains << "\n";

        if      ( actualContains != expectedContains )  { cout << "\n\t FAIL - Contains was wrong" << endl; }
        else                                            { cout << "\n\t PASS" << endl; }
    }   // Test end

    {   // Test begin
        cout << endl << "Rachel Test 2 - PushBack mississippi, check Contains() of z." << endl;
        cout << "\t Prereqs: PushBack(), GetCountOf()" << endl;

        List<string> testList;
        testList.PushBack( "m" );
        testList.PushBack( "i" );
        testList.PushBack( "s" );
        testList.PushBack( "s" );
        testList.PushBack( "i" );
        testList.PushBack( "p" );
        testList.PushBack( "p" );
        testList.PushBack( "i" );

        bool expectedContains = false;
        bool actualContains = testList.Contains( "z" );

        cout << "\t Expected Contains(): " << expectedContains << "\t Actual Contains():   " << actualContains << "\n";

        if      ( actualContains != expectedContains )      { cout << "\n\t FAIL - Contains was wrong" << endl; }
        else                                                { cout << "\n\t PASS" << endl; }
    }   // Test end
}

void Tester::Test_RemoveItem_Rachel()
{
    {
        cout << endl << "Rachel Test 2 - Add 'm', 'i', 's', 's', 'i', 'p', 'p', 'i'. Remove 'i'. Validate results" << endl;
        cout << "\t Prereqs: PushBack(), IsFull()" << endl;

        List<string> testlist;
        testlist.PushBack( "m" );
        testlist.PushBack( "i" );
        testlist.PushBack( "s" );
        testlist.PushBack( "s" );
        testlist.PushBack( "i" );
        testlist.PushBack( "s" );
        testlist.PushBack( "s" );
        testlist.PushBack( "i" );
        testlist.PushBack( "p" );
        testlist.PushBack( "p" );
        testlist.PushBack( "i" );

        testlist.RemoveItem( "i" );

        /*  Expected:   m s s s s p p */
        /*              0 1 2 3 4 5 6 */

        int expectedSize = 7;
        int actualSize = testlist.Size();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << endl;

        string expected[7] = { "m", "s", "s", "s", "s", "p", "p" };
        string actual[7];

        bool allMatch = true;
        for ( int i = 0; i < 7; i++ )
        {
            actual[i] = testlist.ARRAY_NAME[i];
            cout << "\t Expected value at " << i << ": " << expected[i] << "\t Actual value: " << actual[i] << endl;
            if ( actual[i] != expected[i] )
            {
                allMatch = false;
                break;
            }
        }

        if ( expectedSize != actualSize )
        {
            cout << "\n\t FAIL, size is wrong" << endl;
        }
        else if ( !allMatch )
        {
            cout << "\n\t FAIL, values don't match" << endl;
        }
        else
        {
            cout << "\n\t PASS" << endl;
        }
    }
}

void Tester::Test_RemoveIndex_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - PushBack abc, make sure values are right after remove 0" << endl;
        cout << "\t Prereqs: PushBack(), RemoveIndex(), Get(), Size()" << endl;

        List<string> testList;
        testList.PushBack( "a" );
        testList.PushBack( "b" );
        testList.PushBack( "c" );

        testList.RemoveIndex( 0 );

        string expected0 = "b";
        string actual0 = testList.ARRAY_NAME[0];

        string expected1 = "c";
        string actual1 = testList.ARRAY_NAME[1];

        int expectedSize = 2;
        int actualSize = testList.Size();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << "\n";
        cout << "\t Expected value at 0: " << expected0 << "\t Actual value at 0:   " << actual0 << "\n";
        cout << "\t Expected value at 1: " << expected1 << "\t Actual value at 1:   " << actual1 << "\n";

        if      ( actualSize != expectedSize )      { cout << "\n\t FAIL - Size was wrong" << endl; }
        else if      ( actual0 != expected0 )      { cout << "\n\t FAIL - Value at 0 was wrong" << endl; }
        else if      ( actual1 != expected1 )      { cout << "\n\t FAIL - Value at 1 was wrong" << endl; }
        else                                        { cout << "\n\t PASS" << endl; }
    }   // Test end

    {   // Test begin
        cout << endl << "Rachel Test 2 - PushBack abc, make sure values are right after remove 1" << endl;
        cout << "\t Prereqs: PushBack(), RemoveIndex(), Get(), Size()" << endl;

        List<string> testList;
        testList.PushBack( "a" );
        testList.PushBack( "b" );
        testList.PushBack( "c" );

        testList.RemoveIndex( 1 );

        string* buffer;

        string expected0 = "a";
        string actual0 = testList.ARRAY_NAME[0];

        string expected1 = "c";
        string actual1 = testList.ARRAY_NAME[1];

        int expectedSize = 2;
        int actualSize = testList.Size();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << "\n";
        cout << "\t Expected value at 0: " << expected0 << "\t Actual value at 0:   " << actual0 << "\n";
        cout << "\t Expected value at 1: " << expected1 << "\t Actual value at 1:   " << actual1 << "\n";

        if      ( actualSize != expectedSize )      { cout << "\n\t FAIL - Size was wrong" << endl; }
        else if      ( actual0 != expected0 )      { cout << "\n\t FAIL - Value at 0 was wrong" << endl; }
        else if      ( actual1 != expected1 )      { cout << "\n\t FAIL - Value at 1 was wrong" << endl; }
        else                                        { cout << "\n\t PASS" << endl; }
    }   // Test end

    {   // Test begin
        cout << endl << "Rachel Test 3 - PushBack abc, make sure values are right after remove 2" << endl;
        cout << "\t Prereqs: PushBack(), RemoveIndex(), Get(), Size()" << endl;

        List<string> testList;
        testList.PushBack( "a" );
        testList.PushBack( "b" );
        testList.PushBack( "c" );

        testList.RemoveIndex( 2 );

        string* buffer;

        string expected0 = "a";
        string actual0 = testList.ARRAY_NAME[0];

        string expected1 = "b";
        string actual1 = testList.ARRAY_NAME[1];

        int expectedSize = 2;
        int actualSize = testList.Size();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << "\n";
        cout << "\t Expected value at 0: " << expected0 << "\t Actual value at 0:   " << actual0 << "\n";
        cout << "\t Expected value at 1: " << expected1 << "\t Actual value at 1:   " << actual1 << "\n";

        if      ( actualSize != expectedSize )      { cout << "\n\t FAIL - Size was wrong" << endl; }
        else if      ( actual0 != expected0 )      { cout << "\n\t FAIL - Value at 0 was wrong" << endl; }
        else if      ( actual1 != expected1 )      { cout << "\n\t FAIL - Value at 1 was wrong" << endl; }
        else                                        { cout << "\n\t PASS" << endl; }
    }   // Test end
}



void Tester::Test_Insert_Rachel()
{
    {   // Test begin
        cout << endl << "Rachel Test 1 - PushBack abc, insert z at 0, validate" << endl;
        cout << "\t Prereqs: PushBack(), Insert(), Get(), Size()" << endl;

        List<string> testList;
        testList.PushBack( "a" );
        testList.PushBack( "b" );
        testList.PushBack( "c" );

        testList.Insert( 0, "z" );

        string* buffer;

        string expected0 = "z";
        string actual0 = testList.ARRAY_NAME[0];

        string expected1 = "a";
        string actual1 = testList.ARRAY_NAME[1];

        int expectedSize = 4;
        int actualSize = testList.Size();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << "\n";
        cout << "\t Expected value at 0: " << expected0 << "\t Actual value at 0:   " << actual0 << "\n";
        cout << "\t Expected value at 1: " << expected1 << "\t Actual value at 1:   " << actual1 << "\n";

        if      ( actualSize != expectedSize )      { cout << "\n\t FAIL - Size was wrong" << endl; }
        else if      ( actual0 != expected0 )      { cout << "\n\t FAIL - Value at 0 was wrong" << endl; }
        else if      ( actual1 != expected1 )      { cout << "\n\t FAIL - Value at 1 was wrong" << endl; }
        else                                        { cout << "\n\t PASS" << endl; }
    }   // Test end

    {   // Test begin
        cout << endl << "Rachel Test 2 - PushBack abc, insert z at 1, validate" << endl;
        cout << "\t Prereqs: PushBack(), Insert(), Get(), Size()" << endl;

        List<string> testList;
        testList.PushBack( "a" );
        testList.PushBack( "b" );
        testList.PushBack( "c" );

        testList.Insert( 1, "z" );

        string* buffer;

        string expected0 = "a";
        string actual0 = testList.ARRAY_NAME[0];

        string expected1 = "z";
        string actual1 = testList.ARRAY_NAME[1];

        string expected2 = "b";
        string actual2 = testList.ARRAY_NAME[2];

        int expectedSize = 4;
        int actualSize = testList.Size();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << "\n";
        cout << "\t Expected value at 0: " << expected0 << "\t Actual value at 0:   " << actual0 << "\n";
        cout << "\t Expected value at 1: " << expected1 << "\t Actual value at 1:   " << actual1 << "\n";
        cout << "\t Expected value at 2: " << expected2 << "\t Actual value at 2:   " << actual2 << "\n";

        if      ( actualSize != expectedSize )      { cout << "\n\t FAIL - Size was wrong" << endl; }
        else if      ( actual0 != expected0 )      { cout << "\n\t FAIL - Value at 0 was wrong" << endl; }
        else if      ( actual1 != expected1 )      { cout << "\n\t FAIL - Value at 1 was wrong" << endl; }
        else if      ( actual2 != expected2 )      { cout << "\n\t FAIL - Value at 2 was wrong" << endl; }
        else                                        { cout << "\n\t PASS" << endl; }
    }   // Test end

    {   // Test begin
        cout << endl << "Rachel Test 3 - PushBack abc, insert z at 3, validate" << endl;
        cout << "\t Prereqs: PushBack(), Insert(), Get(), Size()" << endl;

        List<string> testList;
        testList.PushBack( "a" );
        testList.PushBack( "b" );
        testList.PushBack( "c" );

        testList.Insert( 3, "z" );

        string* buffer;

        string expected0 = "a";
        string actual0 = testList.ARRAY_NAME[0];

        string expected1 = "b";
        string actual1 = testList.ARRAY_NAME[1];

        string expected2 = "c";
        string actual2 = testList.ARRAY_NAME[2];

        string expected3 = "z";
        string actual3 = testList.ARRAY_NAME[3];

        int expectedSize = 4;
        int actualSize = testList.Size();

        cout << "\t Expected Size(): " << expectedSize << "\t Actual Size():   " << actualSize << "\n";
        cout << "\t Expected value at 0: " << expected0 << "\t Actual value at 0:   " << actual0 << "\n";
        cout << "\t Expected value at 1: " << expected1 << "\t Actual value at 1:   " << actual1 << "\n";
        cout << "\t Expected value at 2: " << expected2 << "\t Actual value at 2:   " << actual2 << "\n";
        cout << "\t Expected value at 3: " << expected3 << "\t Actual value at 3:   " << actual3 << "\n";

        if      ( actualSize != expectedSize )      { cout << "\n\t FAIL - Size was wrong" << endl; }
        else if      ( actual0 != expected0 )       { cout << "\n\t FAIL - Value at 0 was wrong" << endl; }
        else if      ( actual1 != expected1 )       { cout << "\n\t FAIL - Value at 1 was wrong" << endl; }
        else if      ( actual2 != expected2 )       { cout << "\n\t FAIL - Value at 2 was wrong" << endl; }
        else if      ( actual3 != expected3 )       { cout << "\n\t FAIL - Value at 3 was wrong" << endl; }
        else                                        { cout << "\n\t PASS" << endl; }
    }   // Test end
}
