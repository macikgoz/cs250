#ifndef _TESTS
#define _TESTS

#include "List.hpp"

#include <iostream>
using namespace std;

class Tester
{
    public:
    void RunTests();

    private:
    void DrawLine();
    void Test_Init();

    void Test_Size();
    void Test_IsEmpty();
    void Test_IsFull();
    void Test_GetCountOf();
    void Test_Contains();

    void Test_ShiftRight();
    void Test_ShiftLeft();

    void Test_PushFront();
    void Test_PushBack();
    void Test_Insert();

    void Test_PopFront();
    void Test_PopBack();
    void Test_RemoveItem();
    void Test_RemoveIndex();
    void Test_Clear();

    void Test_Get();
    void Test_GetFront();
    void Test_GetBack();

    void Test_RemoveWithValue();
    void Test_RemoveAtIndex();

    // For Rachel to add additional tests for grading
    void Test_Init_Rachel();

    void Test_Size_Rachel();
    void Test_IsEmpty_Rachel();
    void Test_IsFull_Rachel();
    void Test_GetCountOf_Rachel();
    void Test_Contains_Rachel();

    void Test_ShiftRight_Rachel();
    void Test_ShiftLeft_Rachel();

    void Test_PushFront_Rachel();
    void Test_PushBack_Rachel();
    void Test_Insert_Rachel();

    void Test_PopFront_Rachel();
    void Test_PopBack_Rachel();
    void Test_RemoveItem_Rachel();
    void Test_RemoveIndex_Rachel();
    void Test_Clear_Rachel();

    void Test_Get_Rachel();
    void Test_GetFront_Rachel();
    void Test_GetBack_Rachel();

    template <typename T>
    void Debug_ListItems(List<T> item);
};

#endif
